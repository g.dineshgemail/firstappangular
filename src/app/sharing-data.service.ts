import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharingDataService {

  private siblingMsg = new Subject<string>();
  constructor() { }

  public getMessage(): Observable<string> {
    // console.log("Getmethod from sharedata service ", this.siblingMsg.asObservable());
    return this.siblingMsg.asObservable();
  }
  
  public updateMessage(message: string): void {
    this.siblingMsg.next(message);
  }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DasboardHomeComponent } from './dasboard-home/dasboard-home.component';

const routes: Routes = [
  {
    path:'',
    component:DasboardHomeComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }

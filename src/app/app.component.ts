import { Component, ContentChild, ElementRef, HostBinding, ViewChild } from '@angular/core';
import { HeaderComponent } from './header/header.component';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: []
})
export class AppComponent {
  title = 'Wednesday drama';
  age=30;
  flag=false;
  property1: number=100;
  property2: string="Property 2 string";
  Counter=100;
  constructor(private router: Router){
    // this.goToGreen();
  }

  registerUser(form :NgForm){
    console.log("Form data TD : ", form.value);
  }

  btnClicked($event,flag) {
    console.log($event," :flag : " ,flag);
    // alert('button clicked : ',flag);
  }

  goToGreen(){
    this.router.navigate(['../green',{property1: this.property1, property2: this.property2}]);
  }

  // @ViewChild(HeaderComponent) headerComp!: HeaderComponent;
  // ngAfterViewInit() {
  //   console.log(this.headerComp.appendStyle);
  // }

}

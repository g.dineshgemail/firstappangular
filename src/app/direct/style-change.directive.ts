import { Directive, ElementRef, HostBinding, HostListener, OnInit, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appStyleChange]'
})
export class StyleChangeDirective implements OnInit {

  // @HostBinding('style.color') color='yellow';
  constructor(private renderer:Renderer2, private element:ElementRef) {
    console.log("Directive called.");
   }
  ngOnInit(): void {
    // this.element.nativeElement.style.color = "blue";
    this.renderer.setStyle(
      this.element.nativeElement,
      "color",
      "red"
    );
  }
   
  @HostListener('mouseenter') onmouseenter(event: Event){
    this.renderer.setStyle(
      this.element.nativeElement,
      "color",
      "green"
    )
  }
   
  @HostListener('mouseleave') onmouseleave(event: Event){
    this.renderer.setStyle(
      this.element.nativeElement,
      "color",
      "red"
    )
  }
}

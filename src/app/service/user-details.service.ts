import { Injectable } from '@angular/core';
import { LoggerService } from '../service/logger.service';

// @Injectable()

@Injectable()
export class UserDetailsService {

  constructor(private loggerService:LoggerService) { }
  getUserInfo(){
    console.log("Calling User Info Service...");
    this.loggerService.logging();
  }
}

import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoggerService {
  customers = [
    {
      name:"Amy",
      age:30,
      child:[{
        childName:"Alexander"
      }]
    },
    {
      name:"Billy",
      age:30,
      child:[{
        childName:"Rozana"
      }]
    },
    {
      name:"Chris",
      age:30,
      child:[{
        childName:"Abdul Kalam"
      }]
    }
  ];

  constructor() { }
  logging(){
    console.log("This is from logger Service-Logging method : ");
    // return this.customers;
  }

}

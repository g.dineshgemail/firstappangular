import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class ApiCallService {
  // posturl = 'https://jsonplaceholder.typicode.com/todos/1';
  posturl = 'http://localhost:3000/getUsers';
  // posturl = 'http://localhost:3000/getFestivals';

  constructor(private _httpClient : HttpClient) { }
  // getMethod(){
  //   return this._httpClient.get(this.posturl).pipe(map(res => res));
  // }

  // getUsers(){
  //   return this._httpClient.get(this.posturl).pipe(map(res => res));
  // }

  getvip(){
    let url = "http://localhost:3000/getvip";
    return this._httpClient.get(url).pipe(map(res => res));
  }

  // postUsers(data){
  //   let url = 'http://localhost:3000/postUsers';
  //   return this._httpClient.post(url, data).pipe(map(res => res));
  // }

  // getFestivals(){
  //   return this._httpClient.get(this.posturl).pipe(map(res => res));
  // }

}

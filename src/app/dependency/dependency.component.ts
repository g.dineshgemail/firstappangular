import { Component, ElementRef, OnInit, Renderer2 } from '@angular/core';
import { LoggerService } from '../service/logger.service';

@Component({
  selector: 'app-dependency',
  templateUrl: './dependency.component.html',
  styleUrls: ['./dependency.component.scss'],
  providers:[LoggerService]
})
export class DependencyComponent implements OnInit {
  Training = "Nothing";
  customers;
  constructor(private loggerService: LoggerService,private element:ElementRef, private renderer:Renderer2) {
    this.element.nativeElement.addEventListener("click", this.myFunction);
  }
  
  // constructor(private el: ElementRef, private renderer: Renderer) { 
  //   //this.el.nativeElement.style.backgroundColor = "red";
  //   this.el.nativeElement.addEventListener("click", this.myFunction);
  //   //this.el.nativeElement.innerHTML = "<h1>red</h1>";
  //   renderer.setElementStyle(el.nativeElement, 'backgroundColor', 'green');
  // }
  
  myFunction() {
    alert("hi");
  }

  ngOnInit(): void {  }

  logger(){
    // this.customers=
    this.loggerService.logging();
  }

}

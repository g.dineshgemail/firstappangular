import { Component, HostBinding, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  // @HostBinding('style.color') color='orange';
  flat: boolean = true;
  alertStyles = {
    'color': 'red',
    'font-weight': 'bold',
    'borderBottom': '1px solid green'
  };
  blueviolet="blueviolet";
  appendStyle={
    'color':this.blueviolet,
    'font-size':"medium"
  }
  choose = true;
  tab=3;
  customers = [
    {
      name:"Amy",
      age:30,
      child:[{
        childName:"Alexander"
      }]
    },
    {
      name:"Billy",
      age:30,
      child:[{
        childName:"Rozana"
      }]
    },
    {
      name:"Chris",
      age:30,
      child:[{
        childName:"Abdul Kalam"
      }]
    }
  ];

  constructor() { }

  ngOnInit(): void {
  }
}

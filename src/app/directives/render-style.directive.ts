
import { Directive, ElementRef, HostListener, OnInit, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appRenderStyle]'
})
export class RenderStyleDirective implements OnInit{

  constructor(private element: ElementRef, private renderer: Renderer2) { }
  ngOnInit(): void {
    this.renderer.setStyle(
      this.element.nativeElement,
      "color",
      "red"
    )
  }



}

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Data } from '@angular/router';

@Component({
  selector: 'app-green',
  templateUrl: './green.component.html',
  styleUrls: ['./green.component.scss']
})
export class GreenComponent implements OnInit {

  constructor(private activatedRoute:ActivatedRoute, private router: Router) {
    const data: Data = this.activatedRoute.snapshot.data;
    console.log("Green Comp Router Param : ", data);
    console.log("Green Comp state value dynamic: ",this.router.getCurrentNavigation().extras.state);
    console.log("Green Comp state value dynamic: History state : ",history.state);

  }

  ngOnInit(): void {
    console.log("Green Comp : activatedRoute.snapshot.params : ", this.activatedRoute.snapshot.params);
  }

}

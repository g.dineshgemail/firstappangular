import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SharingDataService } from 'src/app/sharing-data.service';

@Component({
  selector: 'app-a',
  templateUrl: './a.component.html',
  styleUrls: ['./a.component.scss'],
  // encapsulation:ViewEncapsulation.None
})
export class AComponent implements OnInit {
  loginForm = new FormGroup({
    username: new FormControl('empty', [Validators.required]),
    // address: new FormGroup({
      city    : new FormControl('England, UK'),
    // }),
    copyUsername    : new FormControl('')
});

login() {
  console.log(this.loginForm.value);
  console.log("User name : ",this.loginForm.value.username);
  // this.loginForm.value.copyUsername = this.loginForm.value.username;
  // console.log(this.loginForm.value);
  // this.loginForm.controls.copyUsername.value = this.loginForm.controls.username.value;
  // this.loginForm.patchValue({
  //   copyUsername: this.loginForm.controls.username.value
  // });
  this.loginForm.setValue({
    username: this.loginForm.value.username,
    city: this.loginForm.value.city,
    copyUsername: this.loginForm.controls.username.value
  });
  console.log(this.loginForm.value);

  // console.log("After : ",this.loginForm.controls.username.value);
  // console.log("form status : ",this.loginForm.status);
  // console.log("form control username status : ",this.loginForm.controls.username.status);
  // this.loginForm.reset();
}

  constructor(private sharingData:SharingDataService) { }

  ngOnInit(): void {
    // this.sendToSibling();
    this.sharingData.updateMessage('This Data is Coming From Child to Sibling - via service');
  }
  public sendToSibling(): void {
    console.log("Updating call : ")
    this.sharingData.updateMessage('This Data is Coming From Child to Sibling - via service');
  }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceInServiceComponent } from './service-in-service.component';

describe('ServiceInServiceComponent', () => {
  let component: ServiceInServiceComponent;
  let fixture: ComponentFixture<ServiceInServiceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ServiceInServiceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceInServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

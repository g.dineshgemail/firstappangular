import { Component, OnInit } from '@angular/core';
import { UserDetailsService } from 'src/app/service/user-details.service';

@Component({
  selector: 'app-service-in-service',
  templateUrl: './service-in-service.component.html',
  styleUrls: ['./service-in-service.component.scss'],
  providers:[UserDetailsService]
})
export class ServiceInServiceComponent implements OnInit {

  constructor(private userDetailsService:UserDetailsService) { }

  ngOnInit(): void {
    this.userDetailsService.getUserInfo();
  }

}

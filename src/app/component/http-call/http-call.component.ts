import { Component, Input, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { ApiCallService } from 'src/app/service/api-call.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-http-call',
  templateUrl: './http-call.component.html',
  styleUrls: ['./http-call.component.scss'],
  // providers:[ApiCallService]
})
export class HttpCallComponent implements OnInit, OnChanges {
  movieObj:any ={
    title:"None",
    userId:0,
    completed: false,
    id: 1
  };
  name="Angular";
  usersObj;
  fest;
  @Input() Counter: number;
  constructor(private apiCallService:ApiCallService) {
    // apiCallService.getMethod().subscribe(data => {
    //   this.movieObj=data;
    //   // this.name=data;
    //   console.log("movieObj : ", this.movieObj);
    // });
  }
  ngOnChanges(changes: SimpleChanges): void {
    // throw new Error('Method not implemented.');
    console.log("In OnChanges from http-call.comp : ",changes);
  }

  ngOnInit(): void {
    console.log("ngOnInit in httpcall : ");
    // this.getvip();
  }

  // getMovie(): void {
  //   this.apiCallService.getMethod().subscribe(data=>{
  //     this.movieObj=data;
  //     console.log("movieObj : ", this.movieObj);
  //   });
  // }

  // getUsers(): void {
  //   this.apiCallService.getUsers().subscribe(data=>{
  //     this.usersObj=data;
  //     console.log("usersObj : ", this.usersObj);
  //   });
  // }

  getvip(): void{
    this.apiCallService.getvip().subscribe(data=>{
      this.usersObj=data;
      // console.log("getvip : ", this.usersObj);
    });
  }

  // loginForm = new FormGroup({
  //   name: new FormControl('enter name', [Validators.required]),
  //   // age    : new FormControl(100),
  // });
  // postUsers(): void {
  //   let data={
  //     name:"Socreties 1",
  //     age:1000
  //   }
  //   console.log(this.loginForm.value);
  //   data = this.loginForm.value;
  //   this.apiCallService.postUsers(data).subscribe(data=>{
  //     this.usersObj=data;
  //     console.log("Post : ", this.usersObj);
  //   });
  // }

  // getFestivals(){
  //   this.apiCallService.getFestivals().subscribe(data=>{
  //     this.fest=data;
  //     console.log("getFestivals : ", this.fest);
  //   });
  // }

}

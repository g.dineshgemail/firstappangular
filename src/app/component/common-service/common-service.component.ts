import { Component, OnInit } from '@angular/core';
import { LogServiceService } from 'src/app/service/log-service.service';

@Component({
  selector: 'app-common-service',
  templateUrl: './common-service.component.html',
  styleUrls: ['./common-service.component.scss'],
  providers:[LogServiceService]
})
export class CommonServiceComponent implements OnInit {
  // 
  constructor(private logServiceService:LogServiceService) {
    // this.commonService=logServiceService;
  }
  commonService;
  ngOnInit(): void {
    this.logServiceService.logger("Common Service.");
  }

}

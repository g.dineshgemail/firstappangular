import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { SharingDataService } from 'src/app/sharing-data.service';

@Component({
  selector: 'app-b',
  templateUrl: './b.component.html',
  styleUrls: ['./b.component.scss']
})
export class BComponent implements OnInit {
  public subscription: Subscription;
  public messageForSibling: string;

  constructor(private shareService:SharingDataService) { }

  public ngOnInit(): void {
    this.shareService.getMessage().subscribe(msg => {
      console.log("Message from sharedData : ",msg);
      this.messageForSibling = msg;
    });
  }

}

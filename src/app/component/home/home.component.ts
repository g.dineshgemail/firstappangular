import { Component, OnInit } from '@angular/core';
// import { LogServiceService } from 'src/app/service/log-service.service';
import { UserDetailsService } from 'src/app/service/user-details.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [UserDetailsService]
})
export class HomeComponent implements OnInit {

  constructor(private UserDetailsService: UserDetailsService) { }

  ngOnInit(): void {
    // let status = "updating logger"
    this.UserDetailsService.getUserInfo();
  }

}

import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Data } from '@angular/router';
@Component({
  selector: 'app-red',
  templateUrl: './red.component.html',
  styleUrls: ['./red.component.scss']
})
export class RedComponent implements OnInit {

  constructor(private activatedRoute:ActivatedRoute, private router:Router) {
    console.log("Red Comp state value : ",this.router.getCurrentNavigation().extras.state);
    // const data:Data = this.activatedRoute.snapshot.data;
    // console.log("Router Param Data : ", data);
  }

  ngOnInit(): void {
    console.log("OnInit Red Comp ",history.state);
  }

}

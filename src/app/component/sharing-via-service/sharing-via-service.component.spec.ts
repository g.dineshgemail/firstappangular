import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SharingViaServiceComponent } from './sharing-via-service.component';

describe('SharingViaServiceComponent', () => {
  let component: SharingViaServiceComponent;
  let fixture: ComponentFixture<SharingViaServiceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SharingViaServiceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SharingViaServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { RenderStyleDirective } from './directives/render-style.directive';
import { StyleChangeDirective } from './direct/style-change.directive';
import { HomeComponent } from './component/home/home.component';
import { UserDetailsService } from './service/user-details.service';
import { LogServiceService } from './service/log-service.service';
import { DependencyComponent } from './dependency/dependency.component';
import { LoggerService } from './service/logger.service';
import { ServiceInServiceComponent } from './component/service-in-service/service-in-service.component';
import { CommonServiceComponent } from './component/common-service/common-service.component';
import { SharingViaServiceComponent } from './component/sharing-via-service/sharing-via-service.component';
import { HttpCallComponent } from './component/http-call/http-call.component'; 
import { HttpClientModule } from '@angular/common/http';
import { RoutingSampleComponent } from './component/routing-sample/routing-sample.component';
import { AComponent } from './component/a/a.component';
import { BComponent } from './component/b/b.component';
import { RedComponent } from './component/red/red.component';
import { GreenComponent } from './component/green/green.component';
import { BlueComponent } from './component/blue/blue.component';
import { AdminComponent } from './component/admin/admin.component';
import { AuthGuard } from './auth.guard';
import { NotFoundComponent } from './component/not-found/not-found.component';
// import { DasboardHomeComponent } from './dashboard/dasboard-home/dasboard-home.component';

const routes: Routes=[
  {
    path:'',
    component:HomeComponent
  },{
    path:'RedComponent',
    component:RedComponent
    // ,
    // data:{tech:"React", version:13}
  },{
    path:'green',
    component:GreenComponent
    // ,
    // data:{tech:"Angular", version:13}
  },{
    path:'blue/:id',
    component:BlueComponent
  },{
    path:'admin',
    component:AdminComponent,
    canActivate:[AuthGuard]
  },
  {
    path: 'dashboard',
    // loadChildren: 'app/dashboard/dashboard.module#DashboardModule'
loadChildren: () => import('../app/dashboard/dashboard.module').then(mod => mod.DashboardModule)
  }
,{
    path:'**',
    component:NotFoundComponent
  }
]

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    RenderStyleDirective,
    StyleChangeDirective,
    HomeComponent,
    DependencyComponent,
    ServiceInServiceComponent,
    CommonServiceComponent,
    SharingViaServiceComponent,
    HttpCallComponent,
    RoutingSampleComponent,
    AComponent,
    BComponent,
    GreenComponent,
    BlueComponent,
    AdminComponent,
    NotFoundComponent,
    // DasboardHomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
